import React, { useEffect, useState } from 'react';
import {Link} from 'react-router-dom';
import {Container,Button,Row,Col,Table} from 'react-bootstrap';
import axios from 'axios';

function GetEmployee(props){
    const [employeeData,setData] = useState([])

    useEffect(()=>{
        async function getData(){
            const res = await axios.get("http://localhost:8081/api/get");
            setData(res.data)
        }
        getData()
    },[]);

    const updateEmployee = (id)=>{
        props.history.push({
            pathname: '/edit/' +id
        })
    }

    const deleteEmployee = (id) =>{
        if (window.confirm("Are you want to delete?")===true){
            axios.delete("http://localhost:8081/api/delete/"+id)
            .then((result)=>{
                setData(employeeData.filter(employee=>employee._id!==id))
            })
            .catch((err)=>{
                console.log(err);
            })
        }else{
            console.log("Nothing");
        }
    }

    return(
        <Container className="mt-4">
            <Link to='/add'><Button variant="light">Add</Button></Link>{' '}
            <Link to='/show'><Button variant="secondary">Show</Button></Link>{' '}
            <Link to='/search'><Button variant="light">Search</Button></Link>{' '}
            <Link to='/searchedData'><Button variant="light">Show Searched data</Button></Link>
            <h3>Employee Data</h3>
            <Row>
                <Col>  
                    <Table>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Employee Name</th>
                                <th>Employee Salary</th>
                                <th>Department</th>
                                <th>Update</th>
                                <th>Delete</th>
                            </tr>
                        </thead>
                        <tbody>
                            {employeeData.map((employee)=>{
                                return(
                                <tr key={employee._id}>
                                    <td>{employee._id}</td>
                                    <td>{employee.name}</td>
                                    <td>{employee.salary}</td>
                                    <td>{employee.department}</td>
                                    <td><Button onClick={()=>{updateEmployee(employee._id)}}>Update</Button></td>
                                    <td><Button onClick={()=>{deleteEmployee(employee._id)}}>Delete</Button></td>
                                </tr>)
                            })}
                        </tbody>
                    </Table>
                </Col>
            </Row>
        </Container>

    )
}
export default GetEmployee;