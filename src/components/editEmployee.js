import React, { useEffect, useState } from 'react';
import { Container, Button, Form, Row, Col } from 'react-bootstrap';
import axios from 'axios';

function EditEmployee(props) {
    const [message, setMessage] = useState("")
    const [data, setData] = useState({})

    useEffect(() => {
        async function getData() {
            const response = await axios.get("http://localhost:8081/api/getEmployee/" + props.match.params.id)
            setData(response.data)
        }
        getData()
    }, [props])

    const [error, setError] = useState({
        nameError: '',
        salaryError: '',
        departmentError: ''
    })

    const validate = () => {
        let nameMessage = "";
        let salaryMessage = "";
        let departmentMessage = "";
        if (!data.name) {
            nameMessage = "Name is Required"
        }

        if (!data.salary) {
            salaryMessage = "Salary is Required"
        }
        if (!data.department) {
            departmentMessage = "Department is Required"
        }

        setError({
            nameError: nameMessage,
            salaryError: salaryMessage,
            departmentError: departmentMessage
        })

        if (nameMessage || salaryMessage || departmentMessage) {
            return false
        } else {
            return true
        }
    }
    const submitData = (e) => {
        e.preventDefault();
        const isValid = validate();
        if (isValid) {
            axios.put('http://localhost:8081/api/update/' + data._id, data)
                .then((result) => {
                    setMessage("Updated Successfully")
                })
                .catch((err) => {
                    setMessage("Operation unsuccessfully")
                })
        }
    }

    const Cancel=()=>{
        props.history.push('/show')
    }
    return (
        <Container className="mt-4">
            <h3>Edit Employee</h3>
            <Row>
                <Col>
                    {message ? <h2>{message}</h2> :
                        <Form onSubmit={submitData}>
                            <Form.Group>
                                <Form.Label>Employee Name: </Form.Label>
                                <Form.Control
                                    type="text"
                                    value={data.name}
                                    onChange={e => setData({ ...data, name: e.target.value })} />
                                <small>{error.nameError}</small>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Employee Salary: </Form.Label>
                                <Form.Control
                                    type="text"
                                    value={data.salary}
                                    onChange={e => setData({ ...data, salary: e.target.value })} />
                                <small>{error.salaryError}</small>
                            </Form.Group>
                            <Form.Group>
                                <Form.Label>Employee Department: </Form.Label>
                                <Form.Control
                                    type="text"
                                    value={data.department}
                                    onChange={e => setData({ ...data, department: e.target.value })} />
                                <small>{error.departmentError}</small>
                            </Form.Group>
                            <Button variant="success" type="submit">Add Employee</Button>{' '}
                            <Button variant="danger" onClick={Cancel}>Cancel</Button>
                        </Form>
                    }
                </Col>
                <Col></Col>
            </Row>
        </Container>
    )
}
export default EditEmployee;