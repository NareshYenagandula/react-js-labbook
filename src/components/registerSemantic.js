import React from 'react';
import {Container,Form,Divider} from 'semantic-ui-react';

function RegisterSemantic(){
    const options = [
        { key: 'm', text: 'Male', value: 'male' },
        { key: 'f', text: 'Female', value: 'female' },
        { key: 'o', text: 'Other', value: 'other' },
      ]
    return(
        <Container>
            <Divider horizontal>Register Semantic UI</Divider>
            <Form>
                <Form.Group>
                    <Form.Input label="First Name" placeholder="First Name" />
                    <Form.Input label="Last Name" placeholder="Last Name" />
                    <Form.Select label="gender" options={options} placeholder="Gender" />
                </Form.Group>
                <Form.Group>
                    <label>Size</label>
                    <Form.Radio label="small" value="sm" />
                    <Form.Radio label="medium" value="md" />
                    <Form.Radio label="Large" value="lg" />
                </Form.Group>
                <Form.TextArea label="About" placeholder="Tell us more about you..." />
                <Form.Checkbox label="I agree to the terms and conditions" />
                <Form.Button>Submit</Form.Button>
            </Form>
        </Container>
    )
}
export default RegisterSemantic;