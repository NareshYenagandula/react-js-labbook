import React from 'react';
import { Container, Row, Col, Form, FormGroup, Button, Input } from 'reactstrap';

function RegisterReactstrap() {
    return (
        <Container>
            <Row>
                <Col></Col>
                <Col>
                    <h3 style={{ textAlign: "center" }}>Register ReactStrap</h3>
                    <Form>
                        <Row form>
                            <Col>
                                <FormGroup>
                                    <Input type="text" placeholder="First Name" />
                                </FormGroup>
                            </Col>
                            <Col>
                                <FormGroup>
                                    <Input type="text" placeholder="Last Name" />
                                </FormGroup>
                            </Col>
                        </Row>
                        <FormGroup>
                            <Input type="email" placeholder="Email" />
                        </FormGroup>
                        <FormGroup>
                            <Input type="password" placeholder="Password" />
                        </FormGroup>
                        <FormGroup>
                            <Input type="password" placeholder="Confirm Password" />
                        </FormGroup>
                        <FormGroup>
                            <Input type="checkbox" />
                            I accept the terms of use & Privacy policy
                        </FormGroup>
                        <Button color="success" >Submit</Button>
                    </Form>
                </Col>
                <Col></Col>
            </Row>
        </Container>
    )
}
export default RegisterReactstrap;