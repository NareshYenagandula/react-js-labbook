import React from 'react';

function FruitType() {
    let fruits = ['Apples','Bluberries','Strawberries','Bananas'];
    return (
        <div>
            <h2>Fruits:</h2>
            <ul>
                <li>Apples</li>
                <li>Blueberries</li>
                <li>Strawberries</li>
                <li>Bananas</li>
                <li>Mangoes</li>
            </ul>
            <h2>Using List and Keys:</h2>
            <ul>
               { fruits.map((item,index) => <li key={index}>{item}</li>) }
            </ul>
        </div>
    )
}
export default FruitType