import React, { Component} from 'react';

export default class Login extends Component {
    render() {
        return(
            <div style={{textAlign:"center"}}>
                <h2 style={{color:"aqua"}}>Log-in to your account</h2>
                <form>
                    <input type='text' placeholder="E-mail Address" style={{padding:"10px 20px",marginBottom:"5px"}} /><br />
                    <input type="password" placeholder="Password" style={{padding:"10px 20px",marginBottom:"5px"}}/><br />
                    <button style={{backgroundColor:"aqua",padding:"10px 20px",border:"0px",borderRadius:"5px"}}>Submit</button>
                </form>
            </div>
        )
    }
}