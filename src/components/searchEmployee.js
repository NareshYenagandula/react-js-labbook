import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Container, Button, Form, Row, Col, Table } from 'react-bootstrap';
import axios from 'axios';

function SearchEmployee() {
    const [data, setData] = useState()
    const [id, setID] = useState();

    const getData = async (e) => {
        e.preventDefault();
        const response = await axios.get("http://localhost:8081/api/getEmployee/" + id)
            setData(response.data)
            console.log(data);
    }
    return (
        <Container className="mt-4">
            <Link to='/add'><Button variant="light">Add</Button></Link>{' '}
            <Link to='/show'><Button variant="light">Show</Button></Link>{' '}
            <Link to='/search'><Button variant="secondary">Search</Button></Link>{' '}
            <Link to='/searchedData'><Button variant="light">Show Searched data</Button></Link>
            <h2>Search Employee</h2>
            <Row>
                <Col>
                    <Form onSubmit={getData}>
                        <Form.Group>
                            <Form.Label>Enter Employee Id to be Searched: </Form.Label>
                            <Form.Control type="text" onChange={e => setID(e.target.value)} />
                        </Form.Group>
                        <Button variant="success" type="submit">Search</Button>
                    </Form>
                    {data ?
                        <React.Fragment>
                            <h3>Employee Data</h3>
                            <Table>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Employee Name</th>
                                        <th>Employee Salary</th>
                                        <th>Department</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{data._id}</td>
                                        <td>{data.name}</td>
                                        <td>{data.salary}</td>
                                        <td>{data.department}</td>
                                    </tr>
                                </tbody>
                            </Table>
                        </React.Fragment>
                        : ""}
                </Col>
                <Col></Col>
            </Row>
        </Container>
    )
}
export default SearchEmployee;