import React,{Component} from 'react';
import {Container,Row,Col,Form,Button} from 'react-bootstrap'

class RegisterBootstrap extends Component{
    constructor(props){
        super(props)
        this.state= {
            firstName:'',
            lastName:'',
            email:'',
            password:'',
            confirmPassword:'',
            agree:"false",
            firstNameError:'',
            lastNameError:'',
            emailError:'',
            passwordError:'',
            confirmPasswordError:''
        }
    }

    updateValue = (e)=>{
        let name = e.target.name;
        let value = e.target.value;
        this.setState({[name]:value});
    }

    validate=()=>{
        let firstNameErrorMessage = '';
        let lastNameErrorMessage='';
        let emailErrorMessage='';
        let passwordErrorMessage='';
        let confirmPasswordErrorMessage='';
        let textPattern = new RegExp(/[A-Za-z]$/);
        let passwordPattern = new RegExp(/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$]).{8,}/);

        if(!this.state.firstName){
            firstNameErrorMessage = "First Name is Required"
        }else if(!textPattern.test(this.state.firstName)){
            firstNameErrorMessage = "Should contain only alphabets"
        }

        if(!this.state.lastName){
            lastNameErrorMessage = "Last Name is Required"
        }else if(!textPattern.test(this.state.lastName)){
            lastNameErrorMessage = "Should contain only alphabets"
        }

        if(!this.state.email){
            emailErrorMessage = "Email is Required"
        }

        if(!this.state.password){
            passwordErrorMessage = "Password is Required"
        }else if(!passwordPattern.test(this.state.password)){
            passwordErrorMessage = "Must contain at least one number and one uppercase and lowercase letter and Special charaters"
        }

        if(!this.state.confirmPassword){
            confirmPasswordErrorMessage = "Confirm Password Required"
        }else if(this.state.password !== this.state.confirmPassword){
            confirmPasswordErrorMessage = "Password & Confirm password must match"
        }
        this.setState({
            firstNameError:firstNameErrorMessage,
            lastNameError:lastNameErrorMessage,
            emailError:emailErrorMessage,
            passwordError:passwordErrorMessage,
            confirmPasswordError:confirmPasswordErrorMessage
        })

        if(firstNameErrorMessage || lastNameErrorMessage || emailErrorMessage || passwordErrorMessage || confirmPasswordErrorMessage){
            return false;
        }else{
            return true;
        }
    }

    submitForm=(e)=>{
        e.preventDefault();
        console.log(this.state.agree);
        const isValid = this.validate();
        if(isValid){
            console.log(this.state);
        }
    }
    render(){
    return(
        <Container>
            <Row>
                <Col></Col>
                <Col>
                <h3 style={{textAlign:"center"}}>Register Boostrap</h3>
                    <Form onSubmit={this.submitForm}>
                        <Form.Group>
                            <Form.Row>
                                <Col>
                                    <Form.Control 
                                        type="text" 
                                        placeholder="First Name" 
                                        name="firstName"
                                        onChange={this.updateValue} />
                                    <small className="text-danger">{this.state.firstNameError}</small>
                                </Col>
                                <Col>
                                <Form.Control 
                                    type="text" 
                                    placeholder="Last Name" 
                                    name="lastName"
                                    onChange={this.updateValue} />
                                <small className="text-danger">{this.state.lastNameError}</small>
                                </Col>
                            </Form.Row>
                        </Form.Group>
                        <Form.Group>
                            <Form.Control 
                                type="email" 
                                placeholder="Email" 
                                name="email"
                                onChange={this.updateValue} />
                            <small className="text-danger">{this.state.emailError}</small>
                        </Form.Group>
                        <Form.Group>
                            <Form.Control 
                                type="password" 
                                placeholder="Password" 
                                name="password"
                                onChange={this.updateValue} />
                            <small className="text-danger">{this.state.passwordError}</small>
                        </Form.Group>
                        <Form.Group>
                            <Form.Control 
                                type="password" 
                                placeholder="Confirm Password" 
                                name="confirmPassword"
                                onChange={this.updateValue} />
                            <small className="text-danger">{this.state.confirmPasswordError}</small>
                        </Form.Group>
                        <Form.Group>
                            <Form.Check
                                type="checkbox"
                                onChange={this.updateValue}
                                name = "agree"
                                value= "true"
                                label="I accept the terms of use & Privacy policy" />
                        </Form.Group>
                        <Button type="submit" variant="success" block>Submit</Button>
                    </Form>
                </Col>
                <Col></Col>
            </Row>
        </Container>
    )
}
}
export default RegisterBootstrap;