import React, { useState } from 'react';
import {Link} from 'react-router-dom';
import {Container,Button,Form,Row,Col} from 'react-bootstrap';
import axios from 'axios';

function AddEmployee(){
    const [message,setMessage] = useState("");
    const [data,setData] = useState({
        id:'',
        name:'',
        salary:'',
        department:''
    })

    const [error,setError] = useState({
        idError:'',
        nameError:'',
        salaryError:'',
        departmentError:''
    })
    
    const validate = () =>{
        let idMessage="";
        let nameMessage="";
        let salaryMessage="";
        let departmentMessage="";
        if(!data.id){
            idMessage="ID is Required"
        }
        if(!data.name){
            nameMessage = "Name is Required"
        }

        if(!data.salary){
            salaryMessage="Salary is Required"
        }
        if(!data.department){
            departmentMessage="Department is Required"
        }

        setError({
            idError:idMessage,
            nameError:nameMessage,
            salaryError:salaryMessage,
            departmentError:departmentMessage
        })

        if(idMessage || nameMessage || salaryMessage || departmentMessage){
            return false
        }else{
            return true
        }
    }
    const submitData=(e)=>{
        e.preventDefault();
        const isValid = validate();
        if(isValid){
            axios.post('http://localhost:8081/api/add',data)
            .then((result)=>{
                setMessage("Added Successfully!!")
            })
            .catch((error)=>{
                setMessage("Operation Unsuccessfull")
            })
        } 
    }
    return(
        <Container className="mt-4">
            <Link to='/add'><Button variant="secondary">Add</Button></Link>{' '}
            <Link to='/show'><Button variant="light">Show</Button></Link>{' '}
            <Link to='/search'><Button variant="light">Search</Button></Link>{' '}
            <Link to='/searchedData'><Button variant="light">Show Searched data</Button></Link>
            <h3>Add Employee</h3>
            <Row>
                <Col>
                    {message?
                    <h2>{message}</h2>:
                    <Form onSubmit={submitData}>
                        <Form.Group>
                            <Form.Label>Employee ID: </Form.Label>
                            <Form.Control type="text" onChange={e=>setData({...data,id:e.target.value})} />
                            <small>{error.idError}</small>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Employee Name: </Form.Label>
                            <Form.Control type="text" onChange={e=>setData({...data,name:e.target.value})} />
                            <small>{error.nameError}</small>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Employee Salary: </Form.Label>
                            <Form.Control type="text" onChange={e=>setData({...data,salary:e.target.value})} />
                            <small>{error.salaryError}</small>
                        </Form.Group>
                        <Form.Group>
                            <Form.Label>Employee Department: </Form.Label>
                            <Form.Control type="text" onChange={e=>setData({...data,department:e.target.value})} />
                            <small>{error.departmentError}</small>
                        </Form.Group>
                        <Button variant="success" type="submit">Add Employee</Button>
                    </Form>
}
                </Col>
                <Col></Col>
            </Row>
        </Container>
    )
}
export default AddEmployee;