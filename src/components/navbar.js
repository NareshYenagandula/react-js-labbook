import React from 'react';
import Radium from 'radium';
import {Link} from 'react-router-dom';

function Navbar() {
    const styles = {
        ul: {
            listStyleType: "none",
            margin: 0,
            padding: 0,
            overflow: "hidden",
            backgroundColor: "#333"
        },
        li: {
            float: "left",
            display: "block",
            color: "white", 
            textAlign: "center",
            padding: "14px 16px",
            textDecoration: "none",
            ':hover': {
                backgroundColor: "aqua",
                color:"black"
            }
        }
    }
    return (
        <div>
            <ul style={styles.ul}>
                <li style={styles.li} key="1"><Link to="/">Lab 2</Link></li>
                <li style={styles.li} key="2"><Link to='/lab3'>Lab 3</Link></li>
                <li style={styles.li} key="3"><Link to="/login">Login</Link></li>
                <li style={styles.li} key="4"><Link to='/registerBoostrap'>Register Boostrap</Link></li>
                <li style={styles.li} key="5"><Link to='/registerReactstrap'>Register ReactStrap</Link></li>
                <li style={styles.li} key="6"><Link to='/registerSemantic'>Register Semantic</Link></li>
                <li style={styles.li} key="7"><Link to='/show'>Employee</Link></li>
            </ul>
        </div>
    )
}
export default Radium(Navbar)