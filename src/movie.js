import React, {Component} from 'react';

export default class Movie extends Component{
    constructor(props){
        super(props)
        this.state = {
            name:this.props.name,
            price:this.props.price
        }
    }
    changeInfo = () =>{
        this.setState({
            name:"Avengers",
            price:"180"
        })
    }
    render(){
        return(
            <div>
                <p>Movie Name: {this.state.name}</p>
                <p>Price: {this.state.price}</p>
                <button onClick={this.changeInfo}>Update</button>
            </div>
        )
    }
}