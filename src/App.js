import './App.css';
import Name from './name';
import Movie from './movie';
import Fruit from './components/fruits';
import Login from './components/login';
import Navbar from './components/navbar';
import RegisterBootstrap from './components/registerBootstrap';
import RegisterReactstrap from './components/registerReactStrap';
import RegisterSemantic from './components/registerSemantic';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import AddEmployee from './components/addEmployee';
import SearchEmployee from './components/searchEmployee';
import GetEmployee from './components/getEmployee';
import EditEmployee from './components/editEmployee';

function App() {
  return (
    <div>
      <BrowserRouter>
        <Navbar />
        <Switch>
          <Route exact path='/'>
            <Name />
          </Route>
          <Route path="/lab3">
            <Movie name="default" price="80" />
            <Fruit />
          </Route>
          <Route path='/login'>
            <Login />
          </Route>
          <Route path='/registerBoostrap'>
            <RegisterBootstrap />
          </Route>
          <Route path="/registerReactstrap">
            <RegisterReactstrap />
          </Route>
          <Route path='/registerSemantic'>
            <RegisterSemantic />
          </Route>
          <Route path='/add' component={AddEmployee} />
          <Route path='/show' component={GetEmployee} />
          <Route path='/search' component={SearchEmployee} />
          <Route path='/edit/:id' component={EditEmployee} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
