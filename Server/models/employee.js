const mongoose = require('mongoose');

const employeeScheme = new mongoose.Schema({
    _id:{
        type:String,
        required:true
    },
    name:{
        type:String,
        required:true
    },
    salary:{
        type:String,
        required:true
    },
    department:{
        type:String,
        required:true
    }
},{_id:false})

module.exports = mongoose.model('employees',employeeScheme);