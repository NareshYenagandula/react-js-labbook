const router = require('express').Router();
const Employee  = require('../models/employee');

router.post('/add', async (req,res)=>{
    //console.log(req.body);
    const employee = new Employee({
        _id:req.body.id,
        name:req.body.name,
        salary:req.body.salary,
        department:req.body.department
    });
    try {
        await employee.save((err,data)=>{
            if(err){
                res.json({message:"Not added"})
            }else{
                res.json(data)
            }
        })
    } catch (error) {
        res.status(400).json({message:"Failed to add employee"}) ;
    }
})

router.get('/get',async (req,res)=>{
    try {
        await Employee.find((err,data)=>{
            if(err){
                console.log(err);
            }else{
                res.json(data);
            }
        })
    } catch (error) {
        console.log("error");
    }
})

router.delete('/delete/:id',async(req,res)=>{
    try {
        await Employee.findByIdAndDelete({_id:req.params.id},(err,data)=>{
            if(err){
                console.log(err);
            }else{
                res.json(data)
            }
        })
    } catch (error) {
        console.log("error while deleting");
    }
})

router.get('/getEmployee/:id',async(req,res)=>{
    try {
        await Employee.findById({_id:req.params.id},(err,data)=>{
            if(err){
                console.log(err);
            }else{
                res.json(data)
            }
        })
    } catch (error) {
        console.log("Error while getting employee details");
    }
})

router.put('/update/:id',async(req,res)=>{
    try {
        await Employee.findByIdAndUpdate(
            {_id:req.params.id},
            {$set:
                {name:req.body.name,
                salary:req.body.salary,
                department:req.body.department}
            },
            {useFindAndModify:false},
            (err,data)=>{
            if(err){
                console.log(err);
            }else{
                res.json(data)
            }
        })
    } catch (error) {
        console.log("Error while updating");
    }
})
module.exports = router;