const express = require('express');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const pageRoute = require('./routes/pageRoute');
const app = express();
const cors = require('cors');

dotenv.config();

mongoose.connect(process.env.DB_CONNECT,{useNewUrlParser:true,useUnifiedTopology: true});
const connection = mongoose.connection;
connection.once('open',()=>{
    console.log("Connected to DB");
})
connection.once('error',()=>{
    console.log("Failed to connect DB");
}) 

app.use(cors());

app.use(express.json());

app.use('/api',pageRoute);

app.get('/',(req,res)=>{
    res.send("Welcome to Employee server");
})

app.listen(process.env.PORT || 8081, ()=>{
    console.log("Server is up");
})